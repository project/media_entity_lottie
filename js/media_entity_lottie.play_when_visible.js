/**
 * @file
 * Behaviours to handle playing animations when visible in viewport.
 */
(function (Drupal, $, once) {

  'use strict';

  Drupal.behaviors.lottiePlayWhenVisible = {
    attach: (context, settings) => {

      $(once('lottie-when-visible', 'lottie-player[play_when_visible="1"]', context))
        .each((index, element) => {
          LottieInteractivity.create({
            player: element,
            mode: 'scroll',
            actions: [
              {
                visibility: [0, 1.0],
                type: 'play'
              }
            ]
          });
        });

    }
  }
}(Drupal, jQuery, once))
